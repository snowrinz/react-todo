/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useState } from 'react';
import 'react-native-gesture-handler';
import { TaskContext } from './contexts/TaskContext';
import HomeScreen from './screens/HomeScreen';
import TodoForm from './screens/TodoForm';
import TodoReminderForm from './screens/TodoReminderForm';

const App = () => {
  const Stack = createStackNavigator();

  const [tasks, setTasks] = useState([
    { id: '1', name: 'Todo 1', date: null },
    { id: '2', name: 'Todo 2', date: new Date() },
    { id: '3', name: 'Todo 3', date: new Date() },
  ]);

  const addTask = (id, name, date) => {
    setTasks(value => {
      return [{ id, name, date }, ...value];
    });
  };

  const updateTask = (id, name, date) => {
    setTasks(value => {
      const modifiedTasks = [...value];
      const oldTodo = modifiedTasks.find(x => x.id === id);
      oldTodo.name = name;
      oldTodo.date = date;
      return [...modifiedTasks];
    });
  };

  const deleteTask = id => {
    setTasks(value => {
      return value.filter(task => task.id !== id);
    });
  };

  const getTask = id => {
    return tasks.find(task => task.id === id);
  };

  return (
    <NavigationContainer>
      <TaskContext.Provider
        value={{ tasks, setTasks, getTask, addTask, updateTask, deleteTask }}>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="TodoForm" component={TodoForm} />
          <Stack.Screen name="TodoReminderForm" component={TodoReminderForm} />
        </Stack.Navigator>
      </TaskContext.Provider>
    </NavigationContainer>
  );
};

export default App;
