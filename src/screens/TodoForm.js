import React, { useContext, useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { TaskContext } from '../contexts/TaskContext';

const TodoForm = ({ navigation, route }) => {
  const { getTask, addTask, updateTask } = useContext(TaskContext);

  const [name, setName] = useState('');

  const id = route.params?.id || -1;

  useEffect(() => {
    if (id > -1) {
      const task = getTask(id);
      setName(task.name);
    }
  }, [getTask, id]);

  return (
    <View>
      <TextInput
        style={styles.input}
        placeholder="Add Todo"
        onChangeText={value => setName(value)}
        value={name}
      />
      <TouchableOpacity
        disabled={name === ''}
        style={[styles.button, name === '' && styles.buttonDisabled]}
        onPress={() => {
          id > -1
            ? updateTask(id, name, null)
            : addTask(new Date().getTime(), name, null);
          navigation.goBack();
        }}>
        <Text style={styles.buttonText}>Add Todo</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 60,
    padding: 8,
    fontSize: 16,
  },
  button: {
    backgroundColor: 'green',
    padding: 9,
    margin: 5,
  },
  buttonDisabled: {
    backgroundColor: 'grey',
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  dateText: {
    fontSize: 20,
  },
});

export default TodoForm;
