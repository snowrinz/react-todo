import React, { useContext, useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import DatePicker from 'react-native-date-picker';
import { TaskContext } from '../contexts/TaskContext';

const TodoReminderForm = ({ navigation, route }) => {
  const [name, setName] = useState('');
  const [date, setDate] = useState(new Date());
  const { getTask, addTask, updateTask } = useContext(TaskContext);

  const id = route.params?.id || -1;

  useEffect(() => {
    if (id > -1) {
      const task = getTask(id);
      setName(task.name);
      setDate(task.date);
    }
  }, [getTask, id]);

  return (
    <View>
      <TextInput
        style={styles.input}
        placeholder="Add Todo"
        onChangeText={value => setName(value)}
        value={name}
      />
      <Text style={styles.dateText}>Date :</Text>
      <DatePicker
        mode="date"
        date={date}
        onDateChange={value => setDate(value)}
      />
      <TouchableOpacity
        disabled={name === ''}
        style={[styles.button, name === '' && styles.buttonDisabled]}
        onPress={() => {
          id > -1
            ? updateTask(id, name, new Date(date))
            : addTask(new Date().getTime(), name, new Date(date));
          navigation.goBack();
        }}>
        <Text style={styles.buttonText}>Add Todo</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 60,
    padding: 8,
    fontSize: 16,
  },
  button: {
    backgroundColor: 'green',
    padding: 9,
    margin: 5,
  },
  buttonDisabled: {
    backgroundColor: 'grey',
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  dateText: {
    fontSize: 20,
  },
});

export default TodoReminderForm;
