/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import Moment from 'moment';
import React, { useContext } from 'react';
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { TaskContext } from '../contexts/TaskContext';

const HomeScreen = ({ navigation }) => {
  const { tasks, deleteTask } = useContext(TaskContext);
  return (
    <>
      <View style={styles.header}>
        <Text style={styles.title}>Todo List</Text>
      </View>
      <View style={styles.container}>
        <Button
          title="Add Todo"
          onPress={() => navigation.navigate('TodoForm')}
        />
        <Button
          title="Add Reminder"
          onPress={() => navigation.navigate('TodoReminderForm')}
        />
      </View>
      <FlatList
        data={tasks}
        keyExtractor={task => task.id}
        renderItem={({ item: task }) => {
          return task.date !== null ? (
            <TouchableOpacity
              style={taskItemStyles.taskItem}
              onPress={() => {
                navigation.navigate('TodoReminderForm', { id: task.id });
              }}>
              <View style={taskItemStyles.taskItemView}>
                <Text style={taskItemStyles.taskItemText}>{task.name}</Text>
                <Icon
                  name="remove"
                  size={20}
                  color="red"
                  onPress={() => deleteTask(task.id)}
                />
              </View>
              <View>
                <Text>{Moment(task.date).format('L')}</Text>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={taskItemStyles.taskItem}
              onPress={() => {
                navigation.navigate('TodoForm', { id: task.id });
              }}>
              <View style={taskItemStyles.taskItemView}>
                <Text style={taskItemStyles.taskItemText}>{task.name}</Text>
                <Icon
                  name="remove"
                  size={20}
                  color="red"
                  onPress={() => deleteTask(task.id)}
                />
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 50,
    paddingTop: 12,
    backgroundColor: 'grey',
  },
  title: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const taskItemStyles = StyleSheet.create({
  taskItem: {
    padding: 15,
    backgroundColor: 'lightgrey',
    borderBottomWidth: 1,
  },
  taskItemView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  taskItemText: {
    fontSize: 15,
  },
});

export default HomeScreen;
